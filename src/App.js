import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Splash from './Pages/Splash';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Pages/Login';

const Stack = createStackNavigator();

const App = () => {
  return (  
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen 
            name="Splash" 
            component={Splash} 
            options={{headerShown: false}} />
        <Stack.Screen 
            name="Login" 
            component={Login} />
      </Stack.Navigator>
    </NavigationContainer>
    
  )
}

export default App

const styles = StyleSheet.create({})
